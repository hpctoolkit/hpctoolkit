// SPDX-FileCopyrightText: Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

#ifndef ITIMER_H
#define ITIMER_H

#include <stdbool.h>
#include "../ompt/ompt-defer.h"

extern void hpcrun_itimer_wallclock_ok(bool flag);

#endif // ITIMER_H
