// SPDX-FileCopyrightText: Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

// -*-Mode: C++;-*-

#include "ragged_vector.hpp"

#include <stdexcept>

using namespace hpctoolkit::util;
