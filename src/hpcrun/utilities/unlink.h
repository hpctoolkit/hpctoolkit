// SPDX-FileCopyrightText: Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

// -*-Mode: C++;-*- // technically C99

/*
 *  Function to recursively unlink a directory hierarchy.
 */

extern int unlink_tree(char *path);
