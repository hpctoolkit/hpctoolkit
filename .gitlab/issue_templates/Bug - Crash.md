<!-- Summarize the crash here. What did you try to do that caused the crash? -->

### Setup

<!-- Provide detailed instructions on how to set up the environment to reproduce the problem. Attach or link any source files needed to demonstrate the issue. If the OS or versions of any dependencies are important, list them here. -->

```console
$ hpcrun --version
```

### To Reproduce

<!-- Provide detailed steps to reproduce the crash. List any commands to run in `console` code blocks. Only include command output that is relevant to the reproduction of the issue. -->

### Logs

<!-- Provide the error message of the crash and any relevant log output in a code block. How frequently does this happen? -->

<details>
<summary>Stack trace from gdb</summary>

```
```

</details>

### Additional Information

<!-- If you have further information, such as additional logs or output files, please attach or link to them here. -->

### Root Cause

<!-- If you have investigated the issue and have some insight into the root cause, please describe any insights here. -->

<!-- Do not remove the following line. -->

/label ~"type::bug"
