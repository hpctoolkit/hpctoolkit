<!-- Summarize the bug here. What are you doing that isn't working as expected? -->

### Setup

<!-- Provide detailed instructions on how to set up the environment to reproduce the problem. Attach or link any source files needed to demonstrate the issue. If the OS or versions of any dependencies are important, say that here. -->

```console
$ hpcrun --version
```

### To Reproduce

<!-- Provide detailed steps to reproduce the issue. List any commands to run in `console` code blocks. Only include command output that is relevant to the reproduction of the issue. -->

### Erroneous Behavior

<!-- What happened that was unexpected? How frequently does it happen? Why is this unexpected? Provide any relevant log output. If the output data is erroneous, provide an excerpt or screenshot of HPCViewer. -->

### Additional Information

<!-- If you have further information, such as full logs or output files, please attach or link to them here. -->

### Root Cause

<!-- If you have investigated the issue and have some insight into the root cause, please describe any insights here. -->

<!-- Do not remove the following line. -->

/label ~"type::bug"
